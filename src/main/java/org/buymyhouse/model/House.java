package org.buymyhouse.model;

import javax.persistence.GeneratedValue;

import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;


@Entity
public class House {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String status;

	private String location;
	private int zipCode;
	private String address;
	private int price;
	
	private String suitableFor;
	private String type;
	private String builtIn;
	private int sizeOfProperty;
	private int numberOfFloors;
	private String garden;
	private String airConditioning;
	private String parking;
	
	private int numberOfRooms;
	private int numberOfBedRooms;
	private int numberOfBathRooms;
	private int numberOfKitchens;
	private int numberOfGarages;
	
	private String nearbySchools;
	private String nearbyHospitals;
	private String nearbyPlayGrounds;
	private String neighbours;
	
	private String linkToPicture;
	private String linkToGoogleMaps;

	private String sellerName;
	private String sellerNumber;
	private String sellerMail;
	private String sellerAvailability;
	
	private String dateOfPosting;
	
	private String description;

	public House()
	{
		
	}
	
	public String getFormattedPrice()
	{

		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
		String currency = format.format(price);
				
		
		
		
		return currency.split("\\.")[0];
		
	}
	
	public boolean isRent()
	{
		if(status.contains("rent"))
			return true;
		
		return false;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getSuitableFor() {
		return suitableFor;
	}

	public void setSuitableFor(String suitableFor) {
		this.suitableFor = suitableFor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBuiltIn() {
		return builtIn;
	}

	public void setBuiltIn(String builtIn) {
		this.builtIn = builtIn;
	}

	public int getSizeOfProperty() {
		return sizeOfProperty;
	}

	public void setSizeOfProperty(int sizeOfProperty) {
		this.sizeOfProperty = sizeOfProperty;
	}

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	public String getGarden() {
		return garden;
	}

	public void setGarden(String garden) {
		this.garden = garden;
	}

	public String getAirConditioning() {
		return airConditioning;
	}

	public void setAirConditioning(String airConditioning) {
		this.airConditioning = airConditioning;
	}

	public String getParking() {
		return parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public int getNumberOfBedRooms() {
		return numberOfBedRooms;
	}

	public void setNumberOfBedRooms(int numberOfBedRooms) {
		this.numberOfBedRooms = numberOfBedRooms;
	}

	public int getNumberOfBathRooms() {
		return numberOfBathRooms;
	}

	public void setNumberOfBathRooms(int numberOfBathRooms) {
		this.numberOfBathRooms = numberOfBathRooms;
	}

	public int getNumberOfKitchens() {
		return numberOfKitchens;
	}

	public void setNumberOfKitchens(int numberOfKitchens) {
		this.numberOfKitchens = numberOfKitchens;
	}

	public int getNumberOfGarages() {
		return numberOfGarages;
	}

	public void setNumberOfGarages(int numberOfGarages) {
		this.numberOfGarages = numberOfGarages;
	}

	public String getNearbySchools() {
		return nearbySchools;
	}

	public void setNearbySchools(String nearbySchools) {
		this.nearbySchools = nearbySchools;
	}

	public String getNearbyHospitals() {
		return nearbyHospitals;
	}

	public void setNearbyHospitals(String nearbyHospitals) {
		this.nearbyHospitals = nearbyHospitals;
	}

	public String getNearbyPlayGrounds() {
		return nearbyPlayGrounds;
	}

	public void setNearbyPlayGrounds(String nearbyPlayGrounds) {
		this.nearbyPlayGrounds = nearbyPlayGrounds;
	}

	public String getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(String neighbours) {
		this.neighbours = neighbours;
	}

	public String getLinkToPicture() {
		return linkToPicture;
	}

	public void setLinkToPicture(String linkToPicture) {
		this.linkToPicture = linkToPicture;
	}

	public String getLinkToGoogleMaps() {
		return linkToGoogleMaps;
	}

	public void setLinkToGoogleMaps(String linkToGoogleMaps) {
		this.linkToGoogleMaps = linkToGoogleMaps;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getSellerNumber() {
		return sellerNumber;
	}

	public void setSellerNumber(String sellerNumber) {
		this.sellerNumber = sellerNumber;
	}

	public String getSellerMail() {
		return sellerMail;
	}

	public void setSellerMail(String sellerMail) {
		this.sellerMail = sellerMail;
	}

	public String getSellerAvailability() {
		return sellerAvailability;
	}

	public void setSellerAvailability(String sellerAvailability) {
		this.sellerAvailability = sellerAvailability;
	}

	public String getDateOfPosting() {
		return dateOfPosting;
	}

	public void setDateOfPosting(String dateOfPosting) {
		this.dateOfPosting = dateOfPosting;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	
	
	
	
	
	
	

}
