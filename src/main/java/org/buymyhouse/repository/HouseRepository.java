package org.buymyhouse.repository;

import java.util.List;

import org.buymyhouse.model.House;
import org.springframework.data.repository.CrudRepository;

public interface HouseRepository extends CrudRepository<House, Long> {
	public House getById(Long id);
	public List<House> findAll();
	public House save(House house);
	public List<House> findByLocationIgnoreCaseContainingOrPrice(String location, int price);
	public List<House> findByLocationIgnoreCaseContainingAndPrice(String location, int price);
	public List<House> findByStatusIgnoreCaseContainingAndLocationIgnoreCaseContaining(String status, String location);
	public List<House> findByStatusIgnoreCaseContaining(String status);
}
