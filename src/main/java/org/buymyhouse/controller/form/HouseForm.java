package org.buymyhouse.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.URL;

public class HouseForm {
	
	private Long id;
	
	@NotNull
	@Size(min = 1)
	private String status;
	
	@NotNull
	@Size(min = 3)
	private String location;
	
	@NotNull
	@Min(value = 1000)
	@Max(value = 9999)
	private int zipCode;
	
	@NotNull
	@Size(min = 5, max = 40)
	private String address;
	
	@NotNull
	@Min(value = 10)
	private int price;
	
	@NotNull
	@Size(min = 1)
	private String suitableFor;
	
	@NotNull
	@Size(min = 1)
	private String type;
	
	@NotNull
	@Size(min = 4)
	private String builtIn;
	
	@NotNull
	@Min(value = 1)
	private int sizeOfProperty;
	
	@NotNull
	@Min(value = 1)
	private int numberOfFloors;
	
	@NotNull
	@Size(min = 1)
	private String garden;
	
	@NotNull
	@Size(min = 1)
	private String airConditioning;
	
	@NotNull
	@Size(min = 1)
	private String parking;
	
	@NotNull
	@Min(value = 1)
	private int numberOfRooms;
	
	@NotNull
	@Min(value = 1)
	private int numberOfBedRooms;

	@NotNull
	@Min(value = 1)
	private int numberOfBathRooms;

	@NotNull
	@Min(value = 0)
	private int numberOfGarages;
	
	@NotNull
	@Min(value = 0)
	private int numberOfKitchens;
	
	@NotNull
	@Size(min = 1)
	private String nearbySchools;
	
	@NotNull
	@Size(min = 1)
	private String nearbyHospitals;
	
	@NotNull
	@Size(min = 1)
	private String nearbyPlayGrounds;
	
	@NotNull
	@Size(min = 1)
	private String neighbours;


	
	@NotNull
	@Size(min = 1)
	private String linkToGoogleMaps;
	
	@NotNull
	@Size(min = 1)
	private String linkToPicture;
	
	
	@NotNull
	@Size(min = 1)
	private String sellerName;
	
	@NotNull
	@Size(min = 1)
	private String sellerNumber;
	
	@NotNull
	@Email
	@Size(min = 1)
	private String sellerMail;
	
	@NotNull
	@Size(min = 1)
	private String sellerAvailability;
	
	private String dateOfPosting;
	
	@NotNull
	@Size(min = 1)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getSuitableFor() {
		return suitableFor;
	}

	public void setSuitableFor(String suitableFor) {
		this.suitableFor = suitableFor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBuiltIn() {
		return builtIn;
	}

	public void setBuiltIn(String builtIn) {
		this.builtIn = builtIn;
	}

	public int getSizeOfProperty() {
		return sizeOfProperty;
	}

	public void setSizeOfProperty(int sizeOfProperty) {
		this.sizeOfProperty = sizeOfProperty;
	}

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	public String getGarden() {
		return garden;
	}

	public void setGarden(String garden) {
		this.garden = garden;
	}

	public String getAirConditioning() {
		return airConditioning;
	}

	public void setAirConditioning(String airConditioning) {
		this.airConditioning = airConditioning;
	}

	public String getParking() {
		return parking;
	}

	public void setParking(String parking) {
		this.parking = parking;
	}

	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public int getNumberOfBedRooms() {
		return numberOfBedRooms;
	}

	public void setNumberOfBedRooms(int numberOfBedRooms) {
		this.numberOfBedRooms = numberOfBedRooms;
	}

	public int getNumberOfBathRooms() {
		return numberOfBathRooms;
	}

	public void setNumberOfBathRooms(int numberOfBathRooms) {
		this.numberOfBathRooms = numberOfBathRooms;
	}

	public int getNumberOfGarages() {
		return numberOfGarages;
	}

	public void setNumberOfGarages(int numberOfGarages) {
		this.numberOfGarages = numberOfGarages;
	}

	public int getNumberOfKitchens() {
		return numberOfKitchens;
	}

	public void setNumberOfKitchens(int numberOfKitchens) {
		this.numberOfKitchens = numberOfKitchens;
	}

	public String getNearbySchools() {
		return nearbySchools;
	}

	public void setNearbySchools(String nearbySchools) {
		this.nearbySchools = nearbySchools;
	}

	public String getNearbyHospitals() {
		return nearbyHospitals;
	}

	public void setNearbyHospitals(String nearbyHospitals) {
		this.nearbyHospitals = nearbyHospitals;
	}

	public String getNearbyPlayGrounds() {
		return nearbyPlayGrounds;
	}

	public void setNearbyPlayGrounds(String nearbyPlayGrounds) {
		this.nearbyPlayGrounds = nearbyPlayGrounds;
	}

	public String getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(String neighbours) {
		this.neighbours = neighbours;
	}

	public String getLinkToGoogleMaps() {
		return linkToGoogleMaps;
	}

	public void setLinkToGoogleMaps(String linkToGoogleMaps) {
		this.linkToGoogleMaps = linkToGoogleMaps;
	}

	public String getLinkToPicture() {
		return linkToPicture;
	}

	public void setLinkToPicture(String linkToPicture) {
		this.linkToPicture = linkToPicture;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getSellerNumber() {
		return sellerNumber;
	}

	public void setSellerNumber(String sellerNumber) {
		this.sellerNumber = sellerNumber;
	}

	public String getSellerMail() {
		return sellerMail;
	}

	public void setSellerMail(String sellerMail) {
		this.sellerMail = sellerMail;
	}

	public String getSellerAvailability() {
		return sellerAvailability;
	}

	public void setSellerAvailability(String sellerAvailability) {
		this.sellerAvailability = sellerAvailability;
	}

	public String getDateOfPosting() {
		return dateOfPosting;
	}

	public void setDateOfPosting(String dateOfPosting) {
		this.dateOfPosting = dateOfPosting;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	

	


	
	
	
	

}
