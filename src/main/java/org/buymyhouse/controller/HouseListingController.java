package org.buymyhouse.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.buymyhouse.controller.form.HouseForm;
import org.buymyhouse.model.House;
import org.buymyhouse.repository.HouseRepository;
import org.buymyhouse.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HouseListingController {

	@Autowired
	HouseService service;
	
	@Autowired
	HouseRepository repository;
	
	@Resource
    private ConversionService conversionService;

	@RequestMapping(value = "/")
	public String index(Model model) {
		model.addAttribute("houses", repository.findAll());
		return "index";

	}
	
	@RequestMapping(value = "/list-houses")
	public String listHouses(@RequestParam(required = false) String status,
							 @RequestParam(required = false) String searchTerm, Model model) {
		
		/*if( ! (status.equals("for sale") || status.equals("to rent") || status.equals("sold")) )
		{
			return "redirect:/";
		}*/
		
		List<House> result;
		
		if(searchTerm.equals(""))
		{
			result = repository.findByStatusIgnoreCaseContaining(status);
		}else
		{
			result = repository.findByStatusIgnoreCaseContainingAndLocationIgnoreCaseContaining(status, searchTerm);
		}
		
		
		model.addAttribute("houses", result);
		return "index";

	}

	@RequestMapping(value = {"/add-house", "/edit-house"}, method = RequestMethod.GET)
	//@ResponseBody
	public String getForm(@RequestParam(required = false) Long houseId, Model model) {
		
		House house;
		
		if(houseId == null)
			house = new House();
		else
		{
			house = repository.getById(houseId);
			//if(house == null) return "redirect:/";
		}
		
		HouseForm form = conversionService.convert(house, HouseForm.class);
		
		model.addAttribute("houseForm", form);
		return "form";


	}
	/*
	@RequestMapping(value = "/edit-house", method = RequestMethod.GET)
	public String editHouse(@RequestParam(required = false) Long houseId, Model model) {
		
		House house;
		
		if(houseId == null)
			return "redirect:/";
		else
		{
			house = repository.getById(houseId);
			if(house == null) return "redirect:/";
		}
		

		HouseForm form = conversionService.convert(house, HouseForm.class);
		//return form.getId() + "";
		model.addAttribute("houseForm", form);
		return "form";


	}
*/
	@RequestMapping(value = "/edit-house", method = RequestMethod.POST)
	public String postForm(@Valid @ModelAttribute("houseForm") HouseForm form, BindingResult result, Model model) {

		if (result.hasErrors()) {
			result.reject("");
			return "form";
			
		}
		
		
		//model.addAttribute("houseForm", form);
		
		House house = conversionService.convert(form, House.class);
		
		house = repository.save(house);
		Long id = house.getId();
		
		model.addAttribute("successMessage", "You have successfully added your house for sale!");
		return "redirect:/view-house?houseId=" + id + "#listing";
	}
	
	
	@RequestMapping(value = "/view-house", method = RequestMethod.GET)
	//@ResponseBody
	public String viewHouse(@RequestParam(required = false) Long houseId, Model model) {
		
		House house;
		
		if(houseId == null)
			return "redirect:/";
		else
		{
			house = repository.getById(houseId);
			if(house == null) return "redirect:/";
		}
		
		model.addAttribute("house", house);
		
		return "view";


	}
	
	@RequestMapping(value = "/search-test", method = RequestMethod.GET)
	@ResponseBody
	public String searchTest(Model model) {
		
		House result = repository.getById((long) 1);
		
		result.setLocation("SETTED");
		repository.save(result);
		
		return result.getLocation();
	}
	
	


}
