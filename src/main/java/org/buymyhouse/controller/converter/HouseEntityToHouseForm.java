package org.buymyhouse.controller.converter;

import org.buymyhouse.controller.form.HouseForm;
import org.buymyhouse.model.House;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class HouseEntityToHouseForm implements Converter<House, HouseForm> {

    @Override
    public HouseForm convert(House source) {
    	HouseForm e = new HouseForm();
    	e.setId(source.getId());
    	e.setStatus(source.getStatus());
    	
        e.setLocation(source.getLocation());
        e.setAddress(source.getAddress());
        e.setZipCode(source.getZipCode());
        e.setPrice(source.getPrice());
        
        e.setSuitableFor(source.getSuitableFor());
        e.setType(source.getType());
        e.setBuiltIn(source.getBuiltIn());
        e.setSizeOfProperty(source.getSizeOfProperty());
        e.setNumberOfFloors(source.getNumberOfFloors());
        e.setGarden(source.getGarden());
        e.setAirConditioning(source.getAirConditioning());
        e.setParking(source.getParking());
        
        e.setNumberOfRooms(source.getNumberOfRooms());
        e.setNumberOfKitchens(source.getNumberOfKitchens());
        e.setNumberOfBathRooms(source.getNumberOfBathRooms());
        e.setNumberOfBedRooms(source.getNumberOfBedRooms());
        e.setNumberOfGarages(source.getNumberOfGarages());
        
        e.setNearbySchools(source.getNearbySchools());
        e.setNearbyHospitals(source.getNearbyHospitals());
        e.setNearbyPlayGrounds(source.getNearbyPlayGrounds());
        e.setNeighbours(source.getNeighbours());
        
        e.setLinkToPicture(source.getLinkToPicture());
        e.setLinkToGoogleMaps(source.getLinkToGoogleMaps());
        
        e.setSellerName(source.getSellerName());
        e.setSellerNumber(source.getSellerNumber());
        e.setSellerMail(source.getSellerMail());
        e.setSellerAvailability(source.getSellerAvailability());
        //e.set(source.get());
        
        e.setDescription(source.getDescription());

       
        
        

        return e;
    }

}
